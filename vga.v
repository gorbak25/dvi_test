`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:38:23 12/26/2018 
// Design Name: 
// Module Name:    vga 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module vga(
	output HS,
	output VS,
	output reg DE,
	output reg pixel_color,
	
	input wire clk
);

parameter H_TOTAL = 800;
parameter H_VISIBLE = 640;
parameter H_SS = 656;
parameter H_SE = 752;

parameter V_TOTAL = 449;
parameter V_VISIBLE = 400;
parameter V_SS = 412;
parameter V_SE = 414;

reg [9:0] hpos;
reg [8:0] vpos;

wire de;

always @(posedge clk) begin
	if (de) begin
		pixel_color <= 1;
		DE <= 1;
	end else begin
		pixel_color <= 0;
		DE <= 0;
	end
end

//assign vy = de ? vpos >> 1 : ((vpos+1) >> 1 >= 200 ?0 : (vpos+1) >> 1);
//assign vx = hpos < H_VISIBLE-1 ? (hpos+1) >> 2 : 0;

initial begin
	hpos = 0;
	vpos = 0;
end

assign HS = (hpos >= H_SS && hpos < H_SE);
assign VS = (vpos >= V_SS && vpos < V_SE);

assign de = hpos < H_VISIBLE && vpos < V_VISIBLE;

always @(posedge clk) begin
	if (hpos == H_TOTAL - 1) begin
		hpos <= 0;
		if (vpos == V_TOTAL - 1)
			vpos <= 0;
		else
			vpos <= vpos + 1;
	end else begin
		hpos <= hpos + 1;
	end
end

endmodule
