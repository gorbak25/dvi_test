`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:11:36 01/22/2019 
// Design Name: 
// Module Name:    uart_test 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module uart_test(
		input clk,
		
		output TX,
		input RX,
		
		output reg[3:0] LED
    );
	 
	 wire uart_busy;
	 reg uart_wr;
	 reg[7:0] uart_dat;
	 
	 uart_transmitter uart_transmitter_instance (
    .uart_busy(uart_busy), 
    .uart_tx(TX), 
    .uart_wr_i(uart_wr), 
    .uart_dat_i(uart_dat), 
    .sys_clk_i(clk), 
    .sys_rst_i(0)
    );
	 
	 wire rx_done;
	 wire[7:0] rx_byte;
	 uart_receiver uart_receiver_instance (
    .uart_data_ready(rx_done), 
    .uart_dat_rx(rx_byte), 
    .uart_rx(RX), 
    .clk(clk), 
    .rst(0)
    );

	initial uart_wr = 0;
	always @(posedge clk) begin
		if(!uart_busy && rx_done) begin
			uart_wr <= 1;
			uart_dat <= rx_byte;
		end else begin
			uart_wr <= 0;
		end
	end


endmodule
