`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:31:57 01/21/2019 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
		input clk,
		output[3:0] DBG_LED,
		
		input[0:0] FPGA_LCD_DB,
		output FPGA_LCD_E,
		output FPGA_LCD_RS,
		output FPGA_LCD_RW,
		
		inout IIC_SDA_VIDEO,
		inout IIC_SCL_VIDEO,
		
		output[11:0] DVI_D,
		output DVI_XCLK_P,
		output DVI_XCLK_N,
		output DVI_HSYNC,
		output DVI_VSYNC,
		output DVI_DE,
		output DVI_RESET_B
    );
	 
	 //vga timing
	 assign DVI_RESET_B = 1;
	 
	 assign DVI_D = 0;
	 
	 wire vclk;
		 /*
	  IN:      100.000 MHz
	  OUT:     25.0 MHZ
	  DESIRED: 25.175 MHz
	*/
	DCM_ADV #(
		.CLKFX_DIVIDE(8),
		.CLKFX_MULTIPLY(2),
		.CLKIN_PERIOD(10.0),
		.CLK_FEEDBACK("NONE"),
		.STARTUP_WAIT("TRUE")
	) dcm_vclk (
		.CLKFX(vclk),
		.CLKIN(clk)
	);
	assign DVI_XCLK_P = vclk;
	assign DVI_XCLK_N = ~vclk;
	
	wire pixel_color;
	vga vga_instance (
    .HS(DVI_HSYNC), 
    .VS(DVI_VSYNC),
	 .DE(DVI_DE),
    .pixel_color(pixel_color), 
    .clk(vclk)
    );
	 
	 
	 
	 //this will enable LCD_DB as inputs
	 assign FPGA_LCD_E = 1;
	 assign FPGA_LCD_RW = 1;
	 
	 wire UART_IN;
	 assign UART_IN = FPGA_LCD_DB;
	 
	 wire UART_OUT;
	 assign FPGA_LCD_RS = UART_OUT;
	 
	 uart_test uart_test_instance (
    .clk(clk), 
    .TX(UART_OUT), 
    .RX(UART_IN)
	 //.LED(DBG_LED)
    );
	 
	 wire rst;
	 wire [6:0]  cmd_address;
	 wire        cmd_start;
	 wire        cmd_read;
	 wire        cmd_write;
	 wire        cmd_write_multiple;
	 wire        cmd_stop;
	 wire        cmd_valid;
	 wire        cmd_ready;
	 wire [7:0]  data_in;
	 wire        data_in_valid;
	 wire        data_in_ready;
	 wire        data_in_last;
	 wire [7:0]  data_out;
	 wire        data_out_valid;
	 wire        data_out_ready;
	 wire        data_out_last;
	 wire        scl_i;
	 wire        scl_o;
	 wire        scl_t;
	 wire        sda_i;
	 wire        sda_o;
	 wire        sda_t;
	 wire        busy;
	 wire        bus_control;
	 wire        bus_active;
	 wire        missed_ack;
	 wire [25:0] prescale;
	 wire        stop_on_idle;
	 
	 //assign prescale = 26'd10000000;
	 assign prescale = 16'd250;
	 assign stop_no_idle = 0;
	 
	assign scl_i = IIC_SCL_VIDEO;
	assign IIC_SCL_VIDEO = scl_t ? 1'bz : scl_o;
	assign sda_i = IIC_SDA_VIDEO;
	assign IIC_SDA_VIDEO = sda_t ? 1'bz : sda_o;
	
	assign DBG_LED[0] = ~IIC_SCL_VIDEO;
	assign DBG_LED[1] = ~IIC_SDA_VIDEO;
	assign DBG_LED[2] = 1;
	assign DBG_LED[3] = 1;
	 
	 i2c_master i2c_master_vga (
    .clk(clk), 
    .rst(rst), 
    .cmd_address(cmd_address), 
    .cmd_start(cmd_start), 
    .cmd_read(cmd_read), 
    .cmd_write(cmd_write), 
    .cmd_write_multiple(cmd_write_multiple), 
    .cmd_stop(cmd_stop), 
    .cmd_valid(cmd_valid), 
    .cmd_ready(cmd_ready), 
    .data_in(data_in), 
    .data_in_valid(data_in_valid), 
    .data_in_ready(data_in_ready), 
    .data_in_last(data_in_last), 
    .data_out(data_out), 
    .data_out_valid(data_out_valid), 
    .data_out_ready(data_out_ready), 
    .data_out_last(data_out_last), 
    .scl_i(scl_i), 
    .scl_o(scl_o), 
    .scl_t(scl_t), 
    .sda_i(sda_i), 
    .sda_o(sda_o), 
    .sda_t(sda_t), 
    .busy(busy), 
    .bus_control(bus_control), 
    .bus_active(bus_active), 
    .missed_ack(missed_ack), 
    .prescale(prescale), 
    .stop_on_idle(stop_on_idle)
    );
	 
	 wire init_busy;
	 wire start;
	 
	 i2c_init i2c_init_chromtel (
    .clk(clk), 
    .rst(rst), 
    .cmd_address(cmd_address), 
    .cmd_start(cmd_start), 
    .cmd_read(cmd_read), 
    .cmd_write(cmd_write), 
    .cmd_write_multiple(cmd_write_multiple), 
    .cmd_stop(cmd_stop), 
    .cmd_valid(cmd_valid), 
    .cmd_ready(cmd_ready), 
    .data_out(data_in), 
    .data_out_valid(data_in_valid), 
    .data_out_ready(data_in_ready), 
    .data_out_last(data_in_last), 
    .busy(init_busy), 
    .start(start)
    );
	 
	 assign start = 1;
	
endmodule
